﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    //Variables
    public static GameManager instance; //The Game Manager
    public PlayerMovement player; //The Player
    public GameObject playerPF; //Player Prefab
    public Vector3 checkpoint;    //The player's current checkpoint
    public int startingLifes;   //The player's starting lives
    public int currentLives;    //The player's current lives

    void Awake()
    {
        if (instance == null)	//If we don't already have an instance, make one.
        {
            instance = this;	// Store THIS instance of the class (component) in the instance variable
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);    //Destroy the extra game manager
            Debug.Log("Warning: A second game manager was detected and destroyed.");	//Warn the designer that they can't add more game managers
        }
    }

    // Use this for initialization
    void Start () {

        checkpoint = new Vector3(0, 0, 0);  //Set the checkpoint
        instance.currentLives = instance.startingLifes; //Set the lives

	}
	
	// Update is called once per frame
	void Update () {

        //Quit Button
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            Application.Quit(); //Quit the game
        }

	}

    public void PlayerDeath()   //When the player dies
    {
        if (currentLives > 0)   //If they have lives left
        {
            currentLives -= 1;  //They lose a life
            Destroy(player.gameObject); //Destroy the player
            Instantiate(playerPF, checkpoint, playerPF.transform.rotation); //Instantiate a new one at the checkpoint
        }
        else
        {
            SceneManager.LoadScene(2);  //Run the gameover scene
        }

    }

    public void PlayGame()
    {
        SceneManager.LoadScene(1);  //Load the game scene
    }

    public void StartScreen()
    {
        SceneManager.LoadScene(0);  //Load the start scene
        instance.currentLives = instance.startingLifes; //Reset the lives
    }

}
