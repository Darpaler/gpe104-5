﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckPoint : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision) //When the player reaches a checkpoint
    {
        GameManager.instance.checkpoint = transform.position;   //Set the checkpoint in the game manager
    }

}
