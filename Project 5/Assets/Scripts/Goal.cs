﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Goal : MonoBehaviour {

    public int sceneToLoad; //The scene you want to load

    private void OnTriggerEnter2D(Collider2D collision) //When they get to the goal
    {
        SceneManager.LoadScene(sceneToLoad);    //Load the Scene
    }

}
