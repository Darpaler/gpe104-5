﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class KillZone : MonoBehaviour {

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")   //If the player falls in
        {
            GameManager.instance.PlayerDeath(); //They die
        }
    }
}
